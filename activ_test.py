#!/usr/bin/python

import json
import sqlite3
import ConfigParser
import sys

configfile = 'config.ini'
json_file = 'ec2_instances.json'

jsonOpen = open(json_file,'r')
json_read = jsonOpen.read()
json_obj = json.loads(json_read)
sec='ec2'

config = ConfigParser.ConfigParser()
config.read(configfile)
#a=config.sections()
db=config.get(sec, 'db_name')
tb=config.get(sec, 'table_name')
cn=config.get(sec, 'columns')
st=config.get(sec, 'instance_state')

#print db, tb

#Creating table in DB
db = db+'.sqlite'

conn = sqlite3.connect(db)

c = conn.cursor()
c.execute('drop table if exists {}'.format(tb))
c.execute('create table {} (id integer primary key)'.format(tb))

cn = cn.split(",")
cn = [x.strip(' ') for x in cn] 
#print cn
for column in cn:
   if column == 'LaunchTime':
      c.execute('alter table {} add column {} date default null'.format (tb, column))
   else:
      c.execute('alter table {} add column {} text default null'.format(tb, column))

