#!/usr/bin/python
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser # ver. < 3.0

#initiate
config = ConfigParser()

#parsing existing ini file
config.read('config.ini')

# reading values from ini file
db_name = config.get('ec2', 'db_name')
table_name = config.get('ec2', 'table_name')
columns  = config.get('ec2', 'columns')
instance_state = config.get('ec2', 'instance_state')

"""create a database in sqlite """
def create_connection(db_name):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_name)
        return conn
    except Error as e:
        print(e)
 
    return None

def create_table(conn, details)
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(details)
    except Error as e:
        print(e)

def main():
    database = sqlite ec2-data
 
    sql_create_table_details = """ CREATE TABLE IF NOT EXISTS projects (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL,
                                        begin_date text,
                                        end_date text
                                    ); """
 
#    sql_create_tasks_table = """CREATE TABLE IF NOT EXISTS tasks (
 #                                   id integer PRIMARY KEY,
 #                                   name text NOT NULL,
 #                                   priority integer,
 #                                   status_id integer NOT NULL,
 #                                   project_id integer NOT NULL,
 #                                   begin_date text NOT NULL,
 #                                   end_date text NOT NULL,
 #                                   FOREIGN KEY (project_id) REFERENCES projects (id)
 #                               );"""
 
    # create a database connection
    conn = create_connection(database)
    if conn is not None:
        # create projects table
        create_table(conn, details)
       """ # create tasks table
        create_table(conn, sql_create_tasks_table)"""
    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()

#print(db_name, instance_state, columns)
