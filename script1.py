#!usr/bin/python
import sqlite
import sqlite3
import sys

conn1 = sqlite3.connect('ec2-data.db')
print "Opened db"

def create_connection('ec2-data'):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect('ec2-data')
        return conn
    except Error as e:
        print(e)
 
    return None

def create_table(conn, create_table_sql)
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def main():
    database = /home/vagrant/python/ec2-data.sqlite
 
    sql_create_details_table = """ CREATE TABLE IF NOT EXISTS projects (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL,
                                        begin_date text,
                                        end_date text
                                    ); """
 
 
    # create a database connection
    conn = create_connection(database)
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_details_table)
    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()

# details table
CREATE TABLE
IF NOT EXISTS projects (
 id integer PRIMARY KEY,
 InstanceId integer NOT NULL,
 InstanceType text,
 PrivateIpAddress text,
 PublicIpAddress text,
 LaunchTime text
);
