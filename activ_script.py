#!/usr/bin/python

import sqlite3
import json
print "Open and read json file"
with open('ec2_instances.json') as json_file:
    data = json.load(json_file)

for IT in data:
    if IT == 'InstanceType' :
         Instance_data = data['InstanceType'][0]
         print Instance_data
for II in data:
    if II == 'InstanceId' :
         InstanceId_data = data['InstanceId'][0]
         print InstanceId_data

conn = sqlite3.connect('ec2-data.db')
c = conn.cursor()

print "Opened database successfully";

conn.execute('''CREATE TABLE IF NOT EXISTS details
       (ID INT PRIMARY KEY     NOT NULL,
       InstanceId           INTEGER    NOT NULL,
       InstanceType            TEXT,
       PrivateIpAddress        TEXT,
       PublicIpAddress         TEXT,
       LaunchTime              TEXT);
       ''')
conn.commit()
print "Table created successfully";
c.execute('INSERT INTO details (InstanceId, InstanceType, PrivateIpAddress, PublicIpAddress, LaunchTime)' 'Values (?,?,?,?,?)' )

conn.close()

c.execute('''SELeCT * FROM DETAILS''')
