#!/usr/bin/env python

import json
import ConfigParser
import sqlite3
import sys
import argparse

def getOpt(config,section,option):
    try:
        x=config.get(section,option)
    except ConfigParser.NoOptionError:
        sys.exit('There is no option %s in section %s' % (section, option))
    return x
    
def idToChange(c,table_name):
    try:
        c.execute('insert into {}(id) values (NULL)'.format(table_name))
    except Exception,e:
        sys.exit('Database error: \n %s' % str(e))
    return int(c.execute('select max(id) from {}'.format(table_name)).fetchone()[0])

def insertRows(c,table_name,valuestuple):
    idtochange = idToChange(c,table_name)
    for key, value in valuestuple.iteritems():
        try:
            c.execute('update {} set {}= ? where id=?'.format(table_name,key),(value,idtochange))
            c.execute('update {} set {}=NULL where {} is NULL '.format(table_name,key,key))
        except Exception,e:
            sys.exit('Database error: \n %s' % str(e))


def main():
    
    parser = argparse.ArgumentParser(description='Inserts data about instances into database')
    parser.add_argument('-s', help='Instance state', required=False,choices=set(('running','stopped','all')))
    arg=vars(parser.parse_args())['s']
    
    configFile='config.ini'
    jsonFile='ec2_instances.json'
    section='ec2'
    
    try:
        jsonOpen = open(jsonFile, 'r')
        jsonString = jsonOpen.read()
        jsonOpen.close()
    except IOError,e:
            if e.errno==2:
                sys.exit('No such file %s' % jsonFile )
            if e.errno==13:
                sys.exit('Permission denied')
            else:
                sys.exit(str(IOError))
        
    try:
        jsonObject = json.loads(jsonString)
    except ValueError as e:
        sys.exit('There is error in your json file. \n %s' % str(e))

    config= ConfigParser.ConfigParser()

    if not config.read(configFile):
        sys.exit('No %s file found' % configFile)

    table_name=getOpt(config,section,'table_name')    
    columns=getOpt(config, section, 'columns')
    db_name=getOpt(config,section,'db_name')

    if arg>1:
        instance_state=arg
    else:
        instance_state=getOpt(config,section,'instance_state')
        
    db_name=db_name+'.sqlite'
    columns=columns.split(",")
    columns = [x.strip(' ') for x in columns] #remove any spaces from column names
               
    try:
        conn = sqlite3.connect(db_name)
        c = conn.cursor()
        c.execute('drop table if exists {}'.format(table_name))
        c.execute('create table {} (id integer primary key)'.format(table_name))
    except Exception,e:
        sys.exit('Database error: \n %s' % str(e))
        

    for column in columns:
        
        if column=='LaunchTime':
            c.execute('alter table {} add column {} date default null'.format(table_name,column))
        else:
            c.execute('alter table {} add column {} text default null'.format(table_name,column))

    for res in jsonObject['Reservations']:
        
        for inst in res['Instances']:
            valuestuple=dict();
            
            for x in columns:
                valuestuple[x]=(inst.get(x, None))

            if inst['State']['Name']==instance_state:
                insertRows(c,table_name,valuestuple)
                    
            if instance_state=="all":
                insertRows(c,table_name,valuestuple)
    try:
        conn.commit()
        conn.close()
    except Exception,e:
        sys.exit('Database error: \n %s' % str(e))                     


if __name__ == '__main__':
    main() 

